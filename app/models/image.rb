class Image < ActiveRecord::Base
  belongs_to :post
  has_attached_file :file, :styles => { :medium => "900x600>", :thumb => "160x120#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :file, :content_type => /\Aimage\/.*\Z/
  # validates_attachment_size :photo, :less_than => 20.megabytes
end
