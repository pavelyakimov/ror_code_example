class Message < ActiveRecord::Base
  validates :subject, presence: true, length: { maximum: 75 }
  validates :email, presence: true
  validates :body, presence: true
end
