class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise  :async,:database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :posts
  
  # Overridden to notify users with password changes
  def after_password_reset
    UserMailer.delay.password_changed_notify(self.id)
  end
  
  def send_reset_password_instructions_notification(token)
    send_devise_notification(:reset_password_instructions, token, {})
    UserMailer.delay.password_changed(self.id)
  end
end
