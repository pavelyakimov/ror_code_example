class Category < ActiveRecord::Base
  has_many :posts
  has_many :subcategories, class_name: "Category", foreign_key: "parent", dependent: :destroy
  belongs_to :parent_category, class_name: "Category"
  
end
