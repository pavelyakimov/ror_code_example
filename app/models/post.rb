class Post < ActiveRecord::Base
  belongs_to :category
  belongs_to :user
  has_many :images, dependent: :destroy
  scope :latest, -> { includes(:images,:category)
                      .order(published_at: :desc)
                      .where(banned: false, deleted: false) }
  scope :user_latest, -> { includes(:images,:category)
                      .order(published_at: :desc)
                      .where(deleted: false) }
  scope :awaiting_moderation, -> { order(published_at: :desc)
                                   .where(moderated: false)}
  
  scope :active, -> { where(banned: false, deleted: false) }
  
  validates :title, presence: true,
                    length: { maximum: 75}
  validates :content, presence: true
  validates :phone, presence: true
  validates :category, presence: true
  validates :user, presence: true
  
  after_create :post_created
  # after_destroy :post_deleted
  
  after_save :perform_index_tasks
  
  searchable :auto_index => false do
    text :title, boost: 5
    text :content
    boolean :banned
    boolean :deleted
    time :published_at
  end
  
  def perform_index_tasks
    Sunspot.index self
  end
  
  def siblings
    newer = Post.active
                .where("published_at > '#{published_at}'")
                .where(category_id: category_id)
                .where("id <> #{id}")
                .order(published_at: :asc)
                .limit(2)
    older = Post.active
                .where("published_at < '#{published_at}'")
                .where(category_id: category_id)
                .where("id <> #{id}")
                .order(published_at: :desc)
                .limit(4 - newer.count)
    newer + older
  end
  
  def publish
    self.published_at = Time.now()
    save
  end
  
  def moderate
    self.moderated = false
    PostMailer.delay.moderate_post(self)
    save
  end
  
  private
  
    def post_created
      PostMailer.delay.post_created(self)
    end
    
    def post_deleted
      PostMailer.delay.post_deleted(self)
    end
    
    def self.latest_published
      self.order(published_at: :desc).first
    end
  
end
