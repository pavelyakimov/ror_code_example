class Admin::MessagesController < Admin::AdminController
  layout 'admin'
  
  def index
    @messages = Message.order id: :desc
  end
end
