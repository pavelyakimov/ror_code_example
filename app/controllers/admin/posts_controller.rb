class Admin::PostsController < Admin::AdminController
  layout 'admin'
  
  def moderate
    @posts = Post.awaiting_moderation
  end
  
end
