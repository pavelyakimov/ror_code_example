class Admin::PagesController < Admin::AdminController
  layout 'admin'
  
  def new
    @page = Page.new
  end

  def create
    @page = Page.new page_params
    if @page.save
      flash[:notice] = 'Страница создана!'
      redirect_to admin_pages_path
    else
      render 'new'
    end
  end

  def update
    @page = Page.find params[:id]
    if @page.update page_params
      flash[:notice] = 'Страница обновлена'
      redirect_to admin_pages_path
    else
      render 'edit'
    end
  end

  def edit
    @page = Page.find params[:id]
  end

  def destroy
    page = Page.find params[:id]
    flash[:notice] = "Страница удалена"
    page.destroy
    redirect_to admin_pages_path
  end

  def index
    @pages = Page.all
  end

  def show
    @page = Page.find params[:id]
  end
  
  private
    def page_params
      params.require(:page).permit(:title,:content)
    end
end
