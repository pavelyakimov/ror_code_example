class PostsController < ApplicationController
  layout :resolve_layout
  
  before_action :set_categories
  before_filter :authenticate_user!, :except => [:index, :show, :banned, :phone, :search]
  
  def phone
    @post = Post.find_by_id params[:post_id]
    if @post
      render plain: @post.phone
    else
      render plain: 'Ошибка! Телефон не найден.'
    end
  end
  
  def search
    @search = Post.search do
      fulltext params[:search]
      with :banned, false
      with :deleted, false
      order_by :published_at, :desc
      paginate :page => params[:page], :per_page => 10
    end
    @posts = @search.results
  end
  
  def index
    @posts = Post.latest.page params[:page]
    flash[:notice] = "Нет объявлений." if @posts.count == 0
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.new post_params
    
    if @post.publish
      flash[:notice] = "Объявление размещено! Теперь Вы можете загрузить фотографии."
      redirect_to user_posts_path
    else
      render 'new'
    end
  end
  
  def success
    @post = Post.find params[:id]
  end

  def images
    @post = Post.find params[:post_id]
  end
  
  def upload
    @post = Post.find params[:post_id]
    # checks if post really belongs to current user
    if @post.user_id == current_user.id
      images = params[:images]
      if images.nil?
        flash[:notice] = "Вы не загрузили ни одного файла!"
        redirect_to post_images_path @post
      else
        counter = 0
        images.each do |file|
          image = @post.images.new
          image.file = file
          if image.save
            counter += 1
          else
            flash[:notice] = "Один или несколько файлов не загружены."
          end
        end
        redirect_to post_path(@post)
      end
    else
      redirect_to root_path
    end
  end

  def destroy_image
    @image = Image.find params[:id]
    owner_id = @image.post.user_id
    if owner_id = current_user.id
      if @image.destroy
        flash[:notice] = "Фотография удалена!"
      else
        flash[:notice] = "Фотография не удалена!"
      end
    else
      flash[:notice] = "Вы не имеете права удалять эту фотографию!"
    end
    redirect_to post_images_path(@image.post.id)
  end

  def edit
    @post = current_user.posts.find params[:id]
  end

  def update
    @post = current_user.posts.find params[:id]
    
    if @post.update(post_params)
      flash[:notice] = "Объявление отредактировано"
      redirect_to user_posts_path
    else
      render 'edit'
    end
  end
  
  def update_date
    @post = current_user.posts.find params[:id]
    if @post
      if @post.published_at < 1.day.ago
        @post.update_columns published_at: Time.now
        if @post.save
          flash[:notice] = "Дата объявления обновлена"
        else
          flash[:notice] = "Ошибка! Дата объявления не обновлена"
        end
      else
        flash[:notice] = "Нельзя поднимать объявление чаще 1 раза в сутки!"
      end
    else
      flash[:notice] = "Ошибка! Объявление не найдено"
    end
    redirect_to user_posts_path
  end

  def moderate
    post = current_user.posts.find params[:id]
    if post.moderate
      flash[:notice] = "Объявление отправлено на проверку."
    else
      flash[:notice] = "Произошла ошибка! Попробуйте отправить объявление на проверку немного позже."
    end
    redirect_to user_posts_path
  end

  def show
    @post = Post.find(params[:id])
    @images = @post.images
    
    if @post.banned
      unless user_signed_in? && current_user.admin?
        redirect_to banned_post_path
      end
    end
  end
  
  def banned
  end

  def destroy
    @post = current_user.posts.find params[:id]
    @post.update_columns deleted: true
    
    flash[:notice] = "Объявление удалено"
    
    redirect_to user_posts_path
  end
  
  def remove_from_db
    if current_user.admin?
      post = Post.find params[:post_id]
      if post.destroy
        flash[:notice] = 'Объявление удалено из БД'
        redirect_to root_path
      else
        flash[:notice] = 'Объявление НЕ удалено из БД!'
        redirect_to post_path(post)
      end
    end
  end
  
  def block
    if current_user.admin?
      post = Post.find params[:post_id]
      post.banned = true
      post.save
      flash[:notice] = 'Объявление заблокировано'
      redirect_to post_path(post)
    else
      flash[:notice] = 'Вы не можете блокировать объявления.'
      redirect_to root_path
    end
  end
  
  def unblock
    if current_user.admin?
      post = Post.find params[:post_id]
      post.banned = false
      post.moderated = true
      post.save
      flash[:notice] = 'Объявление разблокировано'
      redirect_to post_path(post)
    else
      flash[:notice] = 'Вы не можете снять блокировку объявления.'
      redirect_to root_path
    end
  end
  
  private
  
    def set_categories
      @categories = Category.includes(:subcategories).order(title: :asc).where parent: 0
    end
    
    def post_params
      params.require(:post).permit(:title, :content, :phone, :category_id)
    end
    
    def resolve_layout
      case action_name
      when "edit", "images", "new"
        "users"
      when "create"
        "one_column"
      else
        "post"
      end
    end
end
