class PagesController < ApplicationController
  before_action :set_categories
  
  def show
    @page = Page.find params[:id]
  end
  
  def reformal
  end
  
  private
    def set_categories
      @categories = Category.includes(:subcategories).order(title: :asc).where parent: 0
    end
end
