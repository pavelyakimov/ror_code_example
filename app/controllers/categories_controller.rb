class CategoriesController < ApplicationController
  before_action :set_categories
  
  def show
    @category = Category.find params[:id]
    @posts = @category.posts.latest.page params[:page]
    flash[:notice] = "В этой категории нет объявлений." unless @posts.count > 0
  end
  
  private
  
  def set_categories
    @categories = Category.includes(:subcategories).order(title: :asc).where parent: 0
  end
end
