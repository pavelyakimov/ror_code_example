class MessagesController < ApplicationController
  before_action :set_categories
  before_filter :authenticate_user!, :except => [:new, :create, :sent]
  def new
    @message = Message.new
  end

  def create
    @message = Message.new message_params
    if @message.save
      flash[:notice] = "Ваше письмо отправлено!"
      redirect_to root_path
    else
      render 'new'
    end
  end
  
  private
    def set_categories
      @categories = Category.includes(:subcategories).order(title: :asc).where parent: 0
    end
    
    def message_params
      params.require(:message).permit(:subject, :body, :email)
    end
end
