class UsersController < ApplicationController
  layout 'users'
  before_action :authenticate_user!
  before_action :set_categories
  
  def index
    
  end
  
  def show
    if current_user.admin?
      @user = User.find params[:id]
    else
      flash[:notice] = 'Доступ запрещен'
      redirect_to root_path
    end
  end
  
  def delete_posts
    if current_user.admin?
      @user = User.find params[:id]
      @user.posts.each do |p|
        p.destroy
      end
    else
      flash[:notice] = 'Доступ запрещен'
      redirect_to root_path
    end
  end
  
  def posts
    @posts = current_user.posts.user_latest.page params[:page]
  end
  
  def become
    return unless current_user.admin?
    sign_in(:user, User.find(params[:id]))
    redirect_to user_posts_path
  end
  
  def set_categories
    @categories = Category.includes(:subcategories).order(title: :asc).where parent: 0
  end
end
