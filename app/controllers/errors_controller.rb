class ErrorsController < ApplicationController
  before_action :set_categories
  
  def route
    render status: 404
  end
  
  private
    def set_categories
      @categories = Category.includes(:subcategories).order(title: :asc).where parent: 0
    end
end
