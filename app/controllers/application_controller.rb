class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found
  
  
  def after_sign_in_path_for(resource)
    user_posts_path
  end
  
  private
  
    def record_not_found
      # actually that must be refactored in some way...
      render 'errors/route', status: 404
    end
end
