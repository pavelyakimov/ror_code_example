require 'rails_helper'

describe PostsController do
  describe "GET #index" do
    it 'assigns latest posts to @posts' do
      posts = []
      
      5.times do
        posts << create(:post)
      end
      
      get :index
      expect(assigns(:posts)).to match_array(posts)
    end
    
    it 'renders the :index template' do
      get :index
      expect(response).to render_template :index
    end
  end
  
  describe "GET #show" do
    it 'redirects user to banned_post_path if post is banned' do
      post = create(:post, banned: true)
      get :show, id: post
      expect(response).to redirect_to banned_post_path
    end
    
    it 'assigns the requested post to @post' do
      post = create(:post)
      get :show, id: post
      expect(assigns(:post)).to eq(post)
    end
    
    it 'renders the :show template' do
      post = create(:post)
      get :show, id: post
      expect(response).to render_template :show
    end
  end
end
