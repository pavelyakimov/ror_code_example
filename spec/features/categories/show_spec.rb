require 'rails_helper'

describe "Showing categories and it's contents" do
  let!(:category) { create(:category) }
  
  it "shows notice if there is no posts in category" do
    visit category_path category
    expect(page).to have_content("В этой категории нет объявлений.")
    expect(category.posts.count).to eq(0)
  end
end
