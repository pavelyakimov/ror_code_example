require 'rails_helper'

describe "Userpanel posts" do
  let!(:user) { create(:user) }
  let!(:category) { create(:category, title: 'Легковые автомобили') }

  # it authorizes the user
  before(:each) do
    visit new_user_session_path
    fill_in 'user_email', with: user.email
    fill_in 'user_password', with: 'password'
    click_button 'log_in_submit'
    
    expect(page).to have_content('Вход в систему выполнен')
  end
  
  it "shows banned posts" do
    post = create(:post, user: user, banned: true)
    
    visit user_posts_path
    # проверка наличия заголовка забаненного поста
    expect(page).to have_content(post.title)
    # проверка наличия кнопки модерации
    expect(page).to have_content("Модерация")
  end
  
  it "clicks Moderate button" do
    banned_post = create(:post, user: user, banned: true)

    visit user_posts_path
    click_link("post_#{banned_post.id}")
    expect(page).to have_content("Объявление отправлено на проверку.")
  end
end
