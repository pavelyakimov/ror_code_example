require 'rails_helper'

describe "Admin" do
  let!(:admin) { create(:user, admin: true) }

  # it authorizes the user
  before(:each) do
    visit new_user_session_path
    fill_in 'user_email', with: admin.email
    fill_in 'user_password', with: 'password'
    click_button 'log_in_submit'
    
    expect(page).to have_content('Вход в систему выполнен')
  end
  
  it "sees link to admin_root in header" do
    visit root_path
    within(".header .nav") do
      expect(page).to have_content("Админ-панель")
    end
  end
  
end
