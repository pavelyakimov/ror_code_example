require 'rails_helper'

describe "Posts" do
  let!(:admin) { create(:user, admin: true) }
  let!(:category) { create(:category) }

  # it authorizes the user
  before(:each) do
    visit new_user_session_path
    fill_in 'user_email', with: admin.email
    fill_in 'user_password', with: 'password'
    click_button 'log_in_submit'
    
    expect(page).to have_content('Вход в систему выполнен')
  end
  
  it "shows posts links in menu" do
    visit admin_root_path
    within('.sidebar .nav') do 
      expect(page).to have_content("Модерация объявлений")
    end
  end
  
  it "shows how many posts awaiting moderation in link" do
    create(:post, banned: true, moderated: false)
    
    visit admin_root_path
    expect(page.all('.sidebar .nav .awaiting_moderation span').first.text.to_i).to eq(1)
  end
  
  it 'admin sees posts awaiting moderation' do
    post = create(:post, banned: true, moderated: false)
    
    expect(Post.count).to eq(1)
    visit admin_moderate_posts_path
    expect(page).to have_content("Объявления на модерации")
    expect(page).to have_content(post.title)
  end
end
