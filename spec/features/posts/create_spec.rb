require 'rails_helper'

describe "Creating posts" do
  let!(:current_user) { create(:user) }
  let!(:parent_category) { create(:category, title: 'Автомобили') }
  let!(:category) { create(:category, title: 'Легковые автомобили', parent: parent_category.id) }
  
  # it authorizes the user
  before(:each) do
    visit new_user_session_path
    fill_in 'user_email', with: current_user.email
    fill_in 'user_password', with: 'password'
    click_button 'log_in_submit'
    
    expect(page).to have_content('Вход в систему выполнен')
  end
  
  it "checks user can create posts" do
    visit new_post_path
    expect(page).to have_content('Новое объявление')
    
    fill_in 'post_title', with: 'test ad'
    fill_in 'post_content', with: 'test post content'
    fill_in 'post_phone', with: '555555'
    select 'Легковые автомобили', from: 'post_category_id'
    click_button 'submit_post'
    
    expect(page).to have_content('Объявление размещено!')
  end
end
