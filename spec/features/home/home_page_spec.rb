require 'rails_helper'

describe "Home page" do 
  let!(:category) { create(:category) }
  
  it "shows notice if there is no posts" do
    visit root_path
    if Post.all.count == 0
      expect(page).to have_content("Нет объявлений.")
      expect(Post.active.count).to eq(0)
    end
  end
  
  it "shows posts if there are active posts exist" do
    post = create(:post)
    visit root_path
    expect(Post.active.count).to eq(1)
    expect(page).to have_content(post.title)
  end
  
  it "shows search form in sidebar" do
    visit root_path
    expect(page.all(".sidebar .search_form input#search").size).to eq(1)
  end
  
  it "shows search result" do 
    visit root_path
    fill_in "search", with: "дом"
    click_button "start_search"
    expect(page).to have_content("Результаты поиска")
  end
end
