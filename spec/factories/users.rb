require 'faker'

FactoryGirl.define do
  factory :user do
    name { Faker::Name.first_name }
    email { Faker::Internet.safe_email }
    password 'password'
    password_confirmation 'password'
    banned 0
    activated true
    registered Time.now
    confirmed_at Time.now
  end
end
