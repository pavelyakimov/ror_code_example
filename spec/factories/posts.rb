require 'faker'

FactoryGirl.define do
  factory :post do
    title { Faker::Lorem.sentence(2) }
    content 'Post content'
    association :category
    association :user
    phone '5555555'
    published_at Time.now
  end
end
