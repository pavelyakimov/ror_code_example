require 'rails_helper'

describe Post do
  it 'is valid with title, content, phone, user and category' do
    post = build(:post)
    expect(post).to be_valid
  end
  
  it 'is invalid without a title' do
    expect(build(:post, title: nil)).to have(1).errors_on(:title)
  end
  
  it 'is invalid without a content' do
    expect(build(:post, content: nil)).to have(1).errors_on(:content)
  end
  
  it 'is invalid without a phone' do
    expect(build(:post, phone: nil)).to have(1).errors_on(:phone)
  end
  
  it 'is invalid without a category' do
    expect(build(:post, category: nil)).to have(1).errors_on(:category)
  end
  
  it 'is invalid without a user id' do
    expect(build(:post, user: nil)).to have(1).errors_on(:user)
  end
end
